/*
* Angry Pigs Game - UI Manager Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

import { GameStatus } from './Game.js';

export class UIManager {
    constructor() {
        // Inits members
        this.splashScreen = $('#splash-screen');        
        this.levelPicker = $('#level-picker');
        this.startgameBtn = $('#start-game-btn');
        this.modal = $('#modal');
        this.chooseLevelBtn = $('#choose-level-btn');
        this.leftSidebar = $('#left-sidebar');         
        this.ammoCounter = $('#ammo-counter');
        this.birdCounter = $('#bird-counter');
        this.gameArea = $('#game-area');
        this.closeModalBtn = $('#close-modal');
        this.modalTitle = $('#modal-title');

        // Manage the state so we don't draw the same modal more than once
        this.shownNoAmmoModal = false;
        this.shownNoNBirdsModal = false;

        // Attach Listener
        this.closeModalBtn.click(event => this.closeModal(event));
    }

    showSplashScreen() {
        this.shownNoAmmoModal = false;
        this.shownNoNBirdsModal = false;
        this.splashScreen.show();
        this.modal.hide();
        this.leftSidebar.hide();
        this.gameArea.hide();
    }

    showLevelList(data) {
        let markup = '';
        data.levels.forEach(level => markup += `<option value="${level}">${level}</option>`);
        this.levelPicker.html(markup);
    }

    showModal(status) {
        this.splashScreen.hide();
        this.leftSidebar.show();
        this.gameArea.show();
        if(status == GameStatus.NO_AMMO && !this.shownNoAmmoModal) {
            this.closeModalBtn.data('status', GameStatus.NO_AMMO);
            this.modalTitle.html('You ran out of ammo');
            this.modal.show();
        } else if (status == GameStatus.NO_BIRDS  && !this.shownNoNBirdsModal) {
            this.closeModalBtn.data('status', GameStatus.NO_BIRDS);
            this.modalTitle.html('Birds are free!');
            this.modal.show();
        }
    }

    closeModal(event) {
        event.preventDefault();
        let status = this.closeModalBtn.data('status');
        if(status == GameStatus.NO_AMMO) {
            this.shownNoAmmoModal = true;
        } else if (status == GameStatus.NO_BIRDS) {
            this.shownNoNBirdsModal = true;
        }
        this.modal.hide();
    }
    
    showGameArea(background) {
        this.splashScreen.hide();
        this.modal.hide();
        this.leftSidebar.show();
        this.gameArea.css("background-image", "url('./level-editor/images/backgrounds/" + background +"')"); 
        this.gameArea.show();
    }

    update(gameStatus){
        this.ammoCounter.html(gameStatus.ammo);
        this.birdCounter.html(gameStatus.birds);
    }
}
/*
* Angry Pigs Game - Main entry point to the application
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

import { Game } from './Game.js';

// Debug mode switch
// Enables and disables the Debug Draw Mode for the Physics Engine
export const DEBUG_MODE = false;

// Wait for the whole page to load
$(document).ready(() => {
    // Create and run the game instance
    const game = new Game();
    game.start();
});
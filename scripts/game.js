/*
* Angry Pigs Game - Game Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

import { ServerManager } from './ServerManager.js';
import { UIManager } from './UIManager.js';
import { WorldManager } from './WorldManager.js';

export const GameStatus = {
    NO_BIRDS : 0,
    NO_AMMO : 1
};

export class Game {
    constructor() {
        // Members
        this.server = new ServerManager();
        this.ui = new UIManager();
        this.world = new WorldManager();
        this.userPlaying = false;
        // Listeners Attachment
        this.ui.startgameBtn.click(event => this.setupGame());
        this.ui.chooseLevelBtn.click(event => this.resetGame(event));
    }

    // Shows the splash screen & downloads the data from the server
    start() {
        this.server.getLevels()
        .then((data, status, jqrhr) => {
            this.ui.showLevelList(data);
            this.ui.showSplashScreen();
        });
    }

    // Gets the game loaded in JSON format from the server
    // passes the object to the different game components
    setupGame() {
        this.server.loadLevel(this.ui.levelPicker.val())
        .then((data, status, jqxhr) => {
            this.ui.showGameArea(data.background);
            this.world.setup(data);
            this.userPlaying = true;
            this.run();
        }).catch(error => alert('The level could not be loaded, try again'));
    }

    // Sends the userto the main screen
    resetGame(event) {
        event.preventDefault();
        this.userPlaying = false;
        this.world = new WorldManager();
        this.start();
    }

    // Updates the model, then renders and lets the user know the game status
    run() {
        this.world.update();
        this.world.render();
        this.ui.update(this.world.status);
        
        if(this.world.status.ammo == 0 && !this.shownNoAmmoModal) {
            this.ui.showModal(GameStatus.NO_AMMO);
        }
        if(this.world.status.birds == 0 && !this.shownNoNBirdsModal) {
            this.ui.showModal(GameStatus.NO_BIRDS);
        }
        
        window.requestAnimationFrame(() => {
            if (this.userPlaying) this.run();
        });
    }
}
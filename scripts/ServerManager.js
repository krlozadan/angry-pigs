/*
* Angry Pigs Game - Server Manager Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

export class ServerManager {
    constructor() {
        this.endpoint = './level-editor/server/';
    }

    getLevels() {
    let payload = 'action=get-levels';
        return $.post(this.endpoint, payload);
    }

    loadLevel(levelName) {
        let payload = `action=load-level&level-name=${levelName}`;
        return $.post(this.endpoint, payload);
    }
}
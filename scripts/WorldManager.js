/*
* Angry Pigs Game - World Manager Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

import { Physics } from './Physics.js';
import { PhysicsManager } from './PhysicsManager.js';
import { RenderManager } from './RenderManager.js';
import { GameObject, ObjectType } from './GameObject.js';
import { AudioManager } from './AudioManager.js';
import { DEBUG_MODE } from './main.js';

export class WorldManager {
    // Initalization of members
    constructor() {
        this.audioManager = null;
        this.physicsManager = null;
        this.renderManager = null;
        this.level = null;
        this.gameObjects = null;
    }

    // Create the game objects from the JSON file and initializes the listeners
    setup(level) {
        this.level = level; 
        this.status = { ammo : level.ammo, birds : level.birdsCount };
        this.gameObjects = { birds : [], collidables : [], bullets : [], enclosings : [], cannon : null };
        this.audioManager = new AudioManager();
        this.audioManager.playIntroMusic();
        this.renderManager = new RenderManager();
        this.renderManager.canvas.click(event => this.shoot(event));
        this.renderManager.canvas.mousemove(event => this.rotateCannon(event));
        window.addEventListener('birdescaped', event => this.removeBird(event));
        this.physicsManager = new PhysicsManager({ debugDraw : DEBUG_MODE, renderArea : this.renderManager.getRenderAreaSize() });
        this.createEnclosings();
        this.createBirds();
        this.createCollidables();
        this.createCannon();
    }

    // Creation of world limits, birds, collidables and cannon objects

    createEnclosings() {
        for (let i = 0; i < 4; i++) {
            let options = {
                editorScreenSize : this.level.editorScreenSize,
                renderArea : this.renderManager.getRenderAreaSize(),
                type : ObjectType.ENCLOSER,
                scale : Physics.WORLD_SCALE,
                objectInfo : { id : i, posX : 0, posY : 0 }
            };
            let newEncloser = new GameObject(options);
            newEncloser.rigidBody = this.physicsManager.addObject(newEncloser);
            this.gameObjects.enclosings.push(newEncloser);
        }
    }

    createBirds() {
        if(this.level.birdsCount > 0) {
            this.level.birds.forEach(bird => {
                let options = {
                    editorScreenSize : this.level.editorScreenSize,
                    renderArea : this.renderManager.getRenderAreaSize(),
                    type : ObjectType.BIRD,
                    scale : Physics.WORLD_SCALE,
                    objectInfo : bird
                };
                let newBird = new GameObject(options);
                newBird.rigidBody = this.physicsManager.addObject(newBird);
                this.gameObjects.birds.push(newBird);
            });
        }
    }

    createCollidables() {
        if(this.level.collidablesCount > 0) {
            this.level.collidables.forEach(collidable => {
                let options = {
                    editorScreenSize : this.level.editorScreenSize,
                    renderArea : this.renderManager.getRenderAreaSize(),
                    type : ObjectType.COLLIDABLE,
                    scale : Physics.WORLD_SCALE,
                    objectInfo : collidable
                };
                let newCollidable = new GameObject(options);
                newCollidable.rigidBody = this.physicsManager.addObject(newCollidable);
                this.gameObjects.collidables.push(newCollidable);
            });
        }
    }

    createCannon() {
        let options = {
            editorScreenSize : this.level.editorScreenSize,
            renderArea : this.renderManager.getRenderAreaSize(),
            type : ObjectType.CANNON,
            scale : Physics.WORLD_SCALE,
            objectInfo : this.level.cannon
        };
        let cannon = new GameObject(options);
        this.gameObjects.cannon = cannon;
    }

    // After updating the physics engine, we update every model in the world
    update() {
        // Makes the Physics World take a step
        this.physicsManager.update();
        
        if(this.gameObjects.birds.length > 0) {
            this.gameObjects.birds.forEach(bird => {
                bird.rigidBody.GetBody().ApplyImpulse(new Physics.Vec2(0,-0.00001), new Physics.Vec2(0,0));
                bird.updateModel();
            });
        }

        if(this.gameObjects.collidables.length > 0) {
            this.gameObjects.collidables.forEach(collidable => {
                collidable.updateModel();
            });
        }

        if(this.gameObjects.bullets.length > 0) {
            this.gameObjects.bullets.forEach(bullet => {
                bullet.updateModel();
            });
        }
    }

    // Render every element on the screen
    render() {
        this.renderManager.render(this.gameObjects);
    }

    // Removes the bird model from the world
    removeBird(event) {
        let freeBird = event.detail.birdId;
        for (let i = 0; i < this.gameObjects.birds.length; i++) {
            if(this.gameObjects.birds[i].id == freeBird) {
                this.gameObjects.birds.splice(i, 1);
                this.status.birds--;
            }
        }
    }

    /********************************************************************
     * Creates two vectors representing the mouse and the cannon. Finds 
     * the angle between the two. Since our raw image is drawn with the 
     * cannon at a 45* angle, our cannon must be rotated another -45*'s, 
     * or the cannon will not render properly. The cannon's rotation is used
     * purely for visuals, and any game logic would need to rotate the cannon
     * 45*'s clockwise before using.
     *******************************************************************/
    rotateCannon(event) {
        event.preventDefault();
        let mouse = {
            x: event.offsetX,
            y: event.offsetY
        };
        let cannon = {
            x: this.gameObjects.cannon.posX,
            y: this.gameObjects.cannon.posY
        };        

        // Proper visuals require getting angle from center of object.
        cannon.x += this.gameObjects.cannon.width/2;
        cannon.y += this.gameObjects.cannon.height/2;



        let angle = this.physicsManager.findAngleFromWorldCoordinates(mouse, cannon, true);
        angle -= 45;
        this.gameObjects.cannon.rotation = angle;
    }

    shoot(event) {
        event.preventDefault();
        if(this.status.ammo > 0) {
            this.status.ammo--;
            let options = {
                editorScreenSize : this.level.editorScreenSize,
                renderArea : this.renderManager.getRenderAreaSize(),
                type : ObjectType.BULLET,
                scale : Physics.WORLD_SCALE,
                objectInfo : { id : 0, posX : 0, posY : 9 },
                cannonPos : this.gameObjects.cannon
            };
            let newBullet = new GameObject(options);
            newBullet.rigidBody = this.physicsManager.addObject(newBullet);
    
            let msPos = {
                x: event.offsetX,
                y: event.offsetY
            };
    
            let shotImpulse = this.calculateShotImpulse(msPos);
            newBullet.rigidBody.GetBody().ApplyImpulse(new Physics.Vec2(shotImpulse.x, shotImpulse.y), new Physics.Vec2(0, 0));
    
            this.gameObjects.bullets.push(newBullet);

            // play shot in audio
            this.audioManager.shot.play();
        }
    }

    /****************************************************************
     * Finds the raw distance between two vectors (or points), converts
     * to a unit vector. Then multiplies the unit vector with our 
     * constant shooting force, so every shot is equally strong.
    *****************************************************************/
    calculateShotImpulse(mousePos){
        // Y Axis flips
        let xDif = mousePos.x - 0;
        let yDif = this.renderManager.getRenderAreaSize().height - mousePos.y;
        yDif*=-1;

        let shotVector = {
            x: xDif,
            y: yDif  
        };

        let unitVector = this.physicsManager.findUnitVector(shotVector);

        let shotStrength = 50000;

        shotVector.x = unitVector.x * shotStrength;
        shotVector.y = unitVector.y * shotStrength;

        return shotVector;
    }


}
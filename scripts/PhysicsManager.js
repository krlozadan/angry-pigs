/*
* Angry Pigs Game - Physics Manager Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/

'use strict';

import { Physics } from './Physics.js';
import { ObjectType, EncloserPosition } from './GameObject.js';
import { AudioEvents } from './AudioManager.js';

export class PhysicsManager {
    constructor(options) {
        // Init all members
        this.renderArea = options.renderArea;
        this.isDebugEnabled = options.debugDraw;
        this.world = new Physics.World(Physics.GRAVITY, true);
        this.attachContactListeners();
        this.birdsToDelete = [];
        // Setup debug draw
        if(this.isDebugEnabled) {
            this.setupDebugMode();
        }
    }

    // Make sthe physics engine take a step and solve all calculations
    update() {
        if(this.isDebugEnabled) {
            this.world.DrawDebugData();
        }

        // Deletes the birds that have touched the top encloser in a safe way
        if(!this.world.IsLocked() && this.birdsToDelete.length > 0){
            for(let i = 0; i < this.birdsToDelete.length; i++) {
                this.world.DestroyBody(this.birdsToDelete[i].birdBody);
                let birdEscaped = new CustomEvent('birdescaped', { 
                    detail : { birdId : this.birdsToDelete[i].birdId },
                    bubbles : true,
                    cancelable : true
                });
                window.dispatchEvent(birdEscaped);
            }
            this.birdsToDelete = [];
        }

        // Makes the world sync with the frame rate
        // Passes the Physics Calculation and the Position calculation, so we have a smoother Simulation
        this.world.Step(1/60, 10, 10);
        this.world.ClearForces();
    }
    
    addObject(gameObject) {

        let fixDef = new Physics.FixtureDef();
        let bodyDef = new Physics.BodyDef();

        // Depending on the object type the body and the fixture are created
        switch(gameObject.objectType) {
            case ObjectType.BIRD:
                fixDef.density = 0.000001;
                fixDef.friction = 0.5;
                // From the center width , height
                fixDef.shape = new Physics.PolygonShape();
                fixDef.shape.SetAsBox(gameObject.width / Physics.WORLD_SCALE, gameObject.height / Physics.WORLD_SCALE);
                bodyDef.userData = { 
                    objectType : ObjectType.BIRD, 
                    id : gameObject.id
                };
                bodyDef.type = Physics.Body.b2_dynamicBody;
                bodyDef.position.x = gameObject.posX / Physics.WORLD_SCALE;
                bodyDef.position.y = gameObject.posY / Physics.WORLD_SCALE;
                break;
            case ObjectType.COLLIDABLE:
                fixDef.density = gameObject.mass;
                fixDef.friction = gameObject.friction;
                fixDef.restitution = gameObject.bounce;
                // From the center width , height
                fixDef.shape = new Physics.PolygonShape();
                fixDef.shape.SetAsBox(gameObject.width / Physics.WORLD_SCALE, gameObject.height / Physics.WORLD_SCALE);
                bodyDef.userData = { 
                    objectType : ObjectType.COLLIDABLE 
                };
                bodyDef.type = Physics.Body.b2_dynamicBody;
                bodyDef.position.x = gameObject.posX / Physics.WORLD_SCALE;
                bodyDef.position.y = gameObject.posY / Physics.WORLD_SCALE;
                break;
            case ObjectType.BULLET:
                fixDef.density = gameObject.mass;
                fixDef.friction = gameObject.friction;
                fixDef.restitution = gameObject.bounce;
                fixDef.shape = new Physics.CircleShape(gameObject.radius / Physics.WORLD_SCALE);
                bodyDef.userData = { 
                    objectType : ObjectType.BULLET 
                };
                bodyDef.type = Physics.Body.b2_dynamicBody;
                bodyDef.position.x = gameObject.posX / Physics.WORLD_SCALE;
                bodyDef.position.y = gameObject.posY / Physics.WORLD_SCALE;
                break;
            case ObjectType.ENCLOSER:
                fixDef.density = 1;
                fixDef.friction = 0.5;
                // From the center width , height
                fixDef.shape = new Physics.PolygonShape();
                fixDef.shape.SetAsBox(gameObject.width / Physics.WORLD_SCALE, gameObject.height / Physics.WORLD_SCALE);
                bodyDef.userData = { 
                    objectType : ObjectType.ENCLOSER,
                    position : gameObject.position
                };
                bodyDef.type = Physics.Body.b2_staticBody;
                bodyDef.position.x = gameObject.posX / Physics.WORLD_SCALE;
                bodyDef.position.y = gameObject.posY / Physics.WORLD_SCALE;
                break;
        }
        // The new object is added to the world
        return this.world.CreateBody(bodyDef).CreateFixture(fixDef);
    }

    setupDebugMode() {
        let canvas = document.querySelector('#canvas');
        
        canvas.width = this.renderArea.width;
        canvas.height = this.renderArea.height;

        let debugDraw = new Physics.DebugDraw();
        debugDraw.SetSprite(canvas.getContext('2d'));
        debugDraw.SetDrawScale(Physics.WORLD_SCALE);
        debugDraw.SetFlags(Physics.DebugDraw.e_shapeBit | Physics.DebugDraw.e_jointBit);
        this.world.SetDebugDraw(debugDraw);
    }

    attachContactListeners() {
        let contactListener = new Physics.Listener();

        contactListener.BeginContact = (contact) => {
            let object1 = contact.GetFixtureA().GetBody().GetUserData();
            let object2 = contact.GetFixtureB().GetBody().GetUserData();
            
            // Check if a bird collided with the top limit and send it to the "Garbage Collector" of the physics engine
            if( (object1.objectType == ObjectType.BIRD && (object2.objectType == ObjectType.ENCLOSER && object2.position == EncloserPosition.TOP)) || 
                ((object1.objectType == ObjectType.ENCLOSER && object1.position == EncloserPosition.TOP) && object2.objectType == ObjectType.BIRD)) {
                this.birdsToDelete.push({
                    birdBody : object1.objectType == ObjectType.BIRD ? contact.GetFixtureA().GetBody() : contact.GetFixtureB().GetBody(),
                    birdId : object1.objectType == ObjectType.BIRD ? object1.id : object2.id
                });
                // Sends an event to play the sound
                let birdHitTopEncloser = new CustomEvent('worldcollision', { 
                    detail : { audioEvent : AudioEvents.BIRD_ESCAPES },
                    bubbles : true,
                    cancelable : true
                });
                window.dispatchEvent(birdHitTopEncloser);
            }

            // Checks if the bullet is colliding with something to make a sound
            if(object1.objectType == ObjectType.BULLET || object2.objectType == ObjectType.BULLET) {
                let bullet = object1.objectType == ObjectType.BULLET ? contact.GetFixtureA().GetBody() : contact.GetFixtureB().GetBody();
                // Sends an event to play the sound
                if(this.findVectorMagnitude(bullet.GetLinearVelocity()) >= 40) {
                    let bulletHitSomething = new CustomEvent('worldcollision', { 
                        detail : { audioEvent : AudioEvents.PIG_CRATE },
                        bubbles : true,
                        cancelable : true
                    });
                    window.dispatchEvent(bulletHitSomething);
                }
            }

        };

        this.world.SetContactListener(contactListener);
    }


    /*********************************************************************
     * All mathematical functions below assume two dimensional vectors.
     * 
     ********************************************************************/

    findVectorMagnitude(vec){
        return Math.sqrt(vec.x*vec.x + vec.y*vec.y);
    }

    findDotProduct(vec1, vec2){
        return vec1.x*vec2.x + vec1.y*vec2.y;
    }

    // Takes in a vector, makes it a unit vector
    findUnitVector(vec = {x: 0, y: 0}){
        // don't overwrite original object using reference
        let vecCopy = {
            x: vec.x,
            y: vec.y
        };

        // calculate the magnitude of the vector
        let magnitude = Math.sqrt((vecCopy.x*vecCopy.x) + (vecCopy.y*vecCopy.y));

        //divide each component by the magnitude
        vecCopy.x /= magnitude;
        vecCopy.y /= magnitude;

        return vecCopy;
    }

    /******************************************************************************
     * Takes in two vectors. Calculates the dot product, and then uses this to get the 
     * angle using the other dot product formula.
     * a dot b = |a||b|cos(angle).
     * Therefore:
     * angle = acos((a dot b)/|a||b|)
     * 
     * Objects must be passed in the correct order
     *****************************************************************************/
    findAngleBetweenObjects(obj1, obj2){
        let dot = this.findDotProduct(obj1, obj2);

        let obj1Mag = this.findVectorMagnitude(obj1);
        let obj2Mag = this.findVectorMagnitude(obj2);

        let angle = Math.acos(dot/(obj1Mag*obj2Mag));
        angle*=Physics.RAD_2_DEG;

        return angle;
    }

    /******************************************************************************
     * Takes in two vectors. Converts them into a new vector, that represents the 
     * vector from a -> b.
     * Use the dot product between this vector and a y^ unit vector, (0, 1), since this 
     * allows us to find the angle in the users coordinates, or screen space.
     * 
     * We may have to convert to or from a system with a different orientation for the 
     * y axis.
     *****************************************************************************/
    findAngleFromWorldCoordinates(obj1, obj2, convertY = true){
        
        let dif = {
            x: obj1.x - obj2.x,
            y: obj1.y - obj2.y
        };

        if(convertY) dif.y*=-1;

        let yHat = {
            x: 0,
            y: 1
        };

        return this.findAngleBetweenObjects(dif, yHat);
    }

}
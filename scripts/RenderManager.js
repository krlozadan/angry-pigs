/*
* Angry Pigs Game - Render Manager Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/

'use strict';

export class RenderManager{
    constructor(){
        this.renderArea = $('#render-area');
        this.canvas = $('#canvas');
    }

    // Renders every model available in the world
    render(gameObjects) {
        let markup = '';

        if(gameObjects.birds.length > 0) {
            gameObjects.birds.forEach(bird => markup += this.createBirdMarkup(bird));
        }
        if(gameObjects.collidables.length > 0) {
            gameObjects.collidables.forEach(collidable => markup += this.createCollidableMarkup(collidable));
        }
        if(gameObjects.bullets.length > 0) {
            gameObjects.bullets.forEach(bullet => markup += this.createBulletMarkup(bullet));
        }

        markup += this.createCannonMarkup(gameObjects.cannon);
        
        this.renderArea.html(markup);
    }

    createBirdMarkup(bird) {
        return `<div class="game-object bird" style="left:${bird.posX - bird.width}px; top:${bird.posY - bird.height}px;"></div>`;
    }

    createCollidableMarkup(collidable) {
        return `<div class="game-object collidable" style="left:${collidable.posX - collidable.width}px; top:${collidable.posY - collidable.height}px; width:${collidable.width * 2}px; height:${collidable.height * 2}px; background-image:url(./level-editor/images/crates/${collidable.texture}); transform:rotate(${collidable.rotation}deg);"></div>`;
    }

    createBulletMarkup(bullet) {
        return `<div class="game-object bullet" style="left:${bullet.posX - bullet.radius}px; top:${bullet.posY - bullet.radius}px; transform:rotate(${bullet.rotation}deg);"></div>`;
    }

    createCannonMarkup(cannon) {
        return `<div class="game-object cannon" style="left:${cannon.posX}px; top:${cannon.posY}px; transform:rotate(${cannon.rotation}deg);"></div>`;
    }

    getRenderAreaSize() {
        return {
            width : this.renderArea.width(),
            height : this.renderArea.height()
        }
    }
}




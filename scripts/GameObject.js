/*
* Angry Pigs Game - Game Object Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

import { Physics } from './Physics.js';

export const ObjectType = {
    BIRD : 0,
    COLLIDABLE : 1,
    BULLET : 2,
    ENCLOSER : 3,
    CANNON : 4
};

export const EncloserPosition = {
    TOP : 0,
    RIGHT : 1,
    BOTTOM : 2,
    LEFT : 3
};

//theoretically, do something like this:
export class GameObject {

    constructor(options){

        // Init the general values that every game object should have
        this.id = options.objectInfo.id;
        this.posX = options.objectInfo.posX * (options.renderArea.width / options.editorScreenSize.width);
        this.posY = options.objectInfo.posY * (options.renderArea.height / options.editorScreenSize.height);
        this.objectType = options.type;
        this.rigidBody = null;
        
        // Depending on the type of object we customize the creation of every game object
        switch(this.objectType) {
            case ObjectType.BIRD: 
                this.width = 40;
                this.height = 37;
                break;
            case ObjectType.COLLIDABLE: 
                this.width = options.objectInfo.width * (options.renderArea.width / options.editorScreenSize.width);
                this.height = options.objectInfo.height * (options.renderArea.height / options.editorScreenSize.height);
                this.texture = options.objectInfo.texture;
                this.mass = options.objectInfo.mass;
                this.bounce = options.objectInfo.bounce;
                this.friction = options.objectInfo.friction;
                this.rotation = 0;
                break;
            case ObjectType.BULLET: 
                this.posX = options.cannonPos.posX + options.cannonPos.width / 2;
                this.posY = options.cannonPos.posY + options.cannonPos.height / 2;
                this.radius = 30;
                this.mass = 100;
                this.bounce = 0.5;
                this.friction = 0.5;
                this.rotation = 0;
                break;
            case ObjectType.ENCLOSER: 
                this.defineEnclosers(options.renderArea);
                break;
            case ObjectType.CANNON:
                this.width = 192;
                this.height =  192;
                this.rotation = 0;
                break;
        }
    }

    // Takes the physics coordinates and updates the model to be rendered
    updateModel() {
        if(this.rigidBody != null) {
            this.posX = this.rigidBody.GetBody().GetPosition().x * Physics.WORLD_SCALE;
            this.posY = this.rigidBody.GetBody().GetPosition().y * Physics.WORLD_SCALE;

            if(this.objectType == ObjectType.BIRD || this.objectType == ObjectType.COLLIDABLE || this.objectType == ObjectType.BULLET)
            {
                this.rotation = this.rigidBody.GetBody().GetAngle() * Physics.RAD_2_DEG;
            }
        }
    }

    // Creates the boundaries of the level in the physics engine
    defineEnclosers(renderArea) {
        switch(this.id) {
            case EncloserPosition.TOP:
                this.width = renderArea.width / 2;
                this.height = 20;
                this.posX = renderArea.width / 2;
                this.posY = 0 - this.height;
                this.position = EncloserPosition.TOP;
                break;
            case EncloserPosition.RIGHT:
                this.width = 20;
                this.height = renderArea.height / 2;
                this.posX = renderArea.width + this.width;
                this.posY = renderArea.height / 2;
                this.position = EncloserPosition.RIGHT;
                break;
            case EncloserPosition.BOTTOM:
                this.width = renderArea.width / 2;
                this.height = 20;
                this.posX = renderArea.width / 2;
                this.posY = renderArea.height + this.height;
                this.position = EncloserPosition.BOTTOM;
                break;
            case EncloserPosition.LEFT:
                this.width = 20;
                this.height = renderArea.height / 2;
                this.posX = 0 - this.width;
                this.posY = renderArea.height / 2;
                this.position = EncloserPosition.LEFT;
                break;
        }
    }
}
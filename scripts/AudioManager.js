/*
* Angry Pigs Game - Audio Manager Class
* @authors: Copyright (C) 2018 Carlos Adan Cortes De la Fuente & Timothy Coish - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 18/04/2018
*/
'use strict';

export const AudioEvents = {
    PIG_CRATE : 0,
    BIRD_ESCAPES : 1
};

export class AudioManager {
    constructor() {
        // load and convert music tracks.
        this.menuMus = new Howl({ src : ['./Audio/Music/MUS_INTRO.wav'] });
        this.gameMus = new Howl({ src : ['./Audio/Music/MUS_Ambient_Combined.mp3'], loop : true });
        this.hit = new Howl({ src : ['./Audio/SFX/SFX_PIG_HIT.wav'] });
        this.shot = new Howl({ src : ['./Audio/SFX/SFX_SHOT3.wav'] });
        this.playerWon = new Howl({ src : ['./Audio/SFX/SFX_APPLAUSE.wav'] });

        // Listents for every collision i nthe Physics World
        window.addEventListener('worldcollision', event => this.playCollisionSound(event));
    }

    playIntroMusic(){
        this.gameMus.play();
        this.menuMus.play();
    }   

    playCollisionSound(event) {
        switch (event.detail.audioEvent){
            case AudioEvents.BIRD_ESCAPES: 
                this.playerWon.play(); 
                break;
            case AudioEvents.PIG_CRATE:
                this.hit.play();
                break;
        }
    }

}
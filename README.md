# Angry Pigs
Level Editor Made by: Carlos Cortes
Game Made by: Carlos Cortes & Timothy Coish
Version: 1.0

### How to use
--------
- Download the source code
- Open index.html file preferably using a local server development environment such as XAMPP with a SQL database connection
- Once opened. Press play. Aim the turret with the mouse. Press LMB to fire. Try to free the birds.

# *** PG12 T2 Javascript Web Apps  - Angry Pigs Level Editor ***
---------------------------------------
Carlos Adan Cortes
29/03/2018

This project lets the user create, load and update levels using a drag and drop interface with some forms to fill up information

//VFS_Depot/Programming/PG12/pg12carlos/WebServer/angry-pigs-level-editor/


## Synopsis
---------------
This is the level editor that is going to be used as a base for the next project.


# Download/Install
---------------------------------------
https://dev.pgwm.vfs.local/~pg12carlos/angry-pigs-level-editor/


# *** How to use ***
---------------------------------------
The user can start adding elements to the level using the object library by clicking on the elements.
The cannon is the only element that is preloadad due to the fact that there can only be one
If there are any levels created previuosly the user can load them and start working on them 
If the user clicks on an element, it opens the object inspector, where he can modify more properties of that particular object
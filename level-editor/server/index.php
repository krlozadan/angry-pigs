<?php
/** ===============================================================================================
 * AJAX Action handler class
 * @author: Scott Henshawe
 * @copyright: 2015 Kibble Games Inc, in cooperation with VFS
 *
 */

/*
* Angry Pigs Server Side Code - Based on the code provided by Scott Henshawe
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/

// Sets the header information to let the client know its receiving JSON information
header('Content-type:application/json');

class Server /* extends ajax_server */ {

    // Members
    private $debug_mode = TRUE;

    // Class Constructor
    public function __construct() {        
        // Checks if action value exists
        if(isset($_POST["action"]) && !empty($_POST["action"])) { 
            // Security measure to check if the request
            // was made using an xmlhttprequest == AJAX
            // While testing with post man 
            if (!$this->debug_mode) {
                if(!$this->is_ajax()) {
                    $response = $this->is_error("Error: 400 - Bad Request");
                    // Returns the response
                    echo json_encode($response);
                    return 0;
                }
            }
            
            // Get the action requested, make these up as needed
            $action = $_POST["action"];   

            // Switch case for value of action
            switch($action) {     
                case "get-levels":
                    $response = $this->get_levels($_POST);
                    break;
                case "load-level":
                    $response = $this->load_level($_POST);
                    break;
                case "save-level":
                    $response = $this->save_level($_POST);
                    break;
                case "get-backgrounds":
                    $response = $this->get_backgrounds($_POST);
                    break;
                case "get-textures":
                    $response = $this->get_textures($_POST);
                    break;
                default:
                    $response = $this->is_error("Error: 101 - Invalid action");
                    break;
            }

            // Returns the response
            echo json_encode($response);
            return 0;
        }
    }

    # Level requests
    private function get_levels($request) {
        return $this->list_file_names('levels', '/.json/', 'levels');
    }

    private function load_level($request) {
        return json_decode(file_get_contents('./levels/'.$request['level-name'], true));
    }

    private function save_level($request) {
        file_put_contents('./levels/'.$request['name'], $request['level']);
        return Array('message' => 'The level was saved successfully');
    }

    # Images requests
    private function get_backgrounds($request) {
        return $this->list_file_names('../images/backgrounds/', '/.jpg/', 'backgrounds');
    }

    private function get_textures($request) {
        return $this->list_file_names('../images/crates/', '/.png/', 'textures');
    }

    /**
     * Helper method to get file names from a specific path 
     */
    private function list_file_names($path, $regex, $keyName){
        $filesInPath = scandir($path);
        $fileNames = preg_grep($regex, $filesInPath);
        $filesArray = Array($keyName => Array());
        foreach ($fileNames as $name) {
            array_push($filesArray[$keyName], $name);
        }
        return $filesArray;
    }

    // Checks if the request was made using an AJAX request
    private function is_ajax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    /*
     * When we encounter an error the handler should call is error with a message and hand that back
     * as a response to the client
     */
    private function is_error($error_msg) {

        // Create a response array (attrib => value) with the origingal post params to start
        $response = $_POST;

        // Add our error message
        $response["error"] = $error_msg;

        // convert the whole response to a JSON string, then add that string
        // as another element to the return message
        //
        // This lets us see the data coming back as a string in the debugger
        if ($this->debug_mode) {
            $response["json"] = json_encode($response);
        }

        // Respond to the client with a JSON string containing attrib => value pairs encoded
        return $response;
    }
}


// ========================================================================
//
// MAIN Handler to process POST requests
//
$ajax_post_handler = new Server;
?>
/*
* Bird Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

class Bird {
    constructor(birdObj = null) {
    	// Creates a new clean Bird
        if(!birdObj) {
        	this.id = null;
            this.posX = 10;
            this.posY = 10;
        } else { // Fills the bird information with a previuosly created bird
        	this.id = birdObj.id;
            this.posX = birdObj.posX;
            this.posY = birdObj.posY;
        }
    }

    // Creats the markup representation of the class in the level editor
    convertToHTML(){
        return `<div data-id="${this.id}" draggable="true" class="draggable bird" style="left:${this.posX}px; top:${this.posY}px;"></div>`;
    }
}
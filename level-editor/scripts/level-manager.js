/*
* Level Manager Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

// Manages the forms to create, save and load a level
// Updates the information displayed in the form
class LevelManager {
	constructor(){
		this.levelPulldown = $('#levels-pulldown');
        this.backgroundPulldown = $('#backgrounds-pulldown');
        this.newLevelBtn = $('#new-level-btn');
        this.loadLevelForm = $('#load-level-form');
        this.saveLevelForm = $('#save-level-form');
	}

	populateLevels(levels){
        let markup = '';
        levels.forEach(level => {
            markup += `<option value="${level}">${level}</option>`;
        });
		this.levelPulldown.html(markup);
	}

	populateBackgroundImages(images){
        let markup = '';
        images.forEach(image => {
            markup += `<option value="${image}">${image}</option>`;
        });
        this.backgroundPulldown.html(markup);
	}

    updateCollidablesCount(collidablesCount){
        $('input[name=collidablesCount]').val(collidablesCount);
    }

    updateBirdsCount(birdsCount){
        $('input[name=birdsCount]').val(birdsCount);
    }

    fillForm(level) {
        $('input[name=name]').val(level.name);
        $('input[name=ammo]').val(level.ammo);    
        $('input[name=collidablesCount]').val(level.collidablesCount);    
        $('input[name=birdsCount]').val(level.birdsCount);
        this.backgroundPulldown.val(level.background);
    }
}
/*
* Cannon Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

class Cannon {
    constructor(cannonObj = null) {
    	// Creates a new cannon
        if(!cannonObj) {
            this.posX = 10;
            this.posY = 10;
        } else { // Fills the cannon information with a previuosly created one
            this.posX = cannonObj.posX;
            this.posY = cannonObj.posY;
        }
    }

    // Creats the markup representation of the class in the level editor
    convertToHTML(){
        return `<div draggable="true" class="draggable cannon" style="left:${this.posX}px; top:${this.posY}px;"></div>`;
    }
}
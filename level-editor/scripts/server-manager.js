/*
* Server Manager Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

// Creates all the requests sent to the server
class ServerManager {
    constructor() {
        this.endpoint = './server/';
    }

    getLevels() {
    let payload = 'action=get-levels';
        return $.post(this.endpoint, payload);
    }

    getBackgroundImages() {
        let payload = 'action=get-backgrounds';
        return $.post(this.endpoint, payload);
    }

    getCollidablesTextures() {
        let payload = 'action=get-textures';
        return $.post(this.endpoint, payload);
    }

    loadLevel(levelName) {
        let payload = `action=load-level&${levelName}`;
        return $.post(this.endpoint, payload);
    }

    saveLevel(level){
        let payload = `action=save-level&name=${level.name}.json&level=${JSON.stringify(level)}`;
        return $.post(this.endpoint, payload);
    }
}
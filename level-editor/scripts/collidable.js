/*
* Collidable Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

class Collidable {
    constructor(collidableObj = null) {
    	// Creates a new clean Collidable
        if(!collidableObj) {
        	this.id = null;
            this.posX = 10;
            this.posY = 10;
            this.height = 80;
            this.width = 80;
            this.texture = "crate_01.png";
            this.mass = 90;
            this.bounce = 0;
            this.friction = 1;
        } else { // Fills the collidable information with a previuosly created collidable
        	this.id = collidableObj.id;
            this.posX = collidableObj.posX;
            this.posY = collidableObj.posY;
            this.height = collidableObj.height;
            this.width = collidableObj.width;
            this.texture = collidableObj.texture;
            this.mass = collidableObj.mass;
            this.bounce = collidableObj.bounce;
            this.friction = collidableObj.friction;
        }
    }

    // Creats the markup representation of the class in the level editor
    convertToHTML(){
    	return `<div data-id="${this.id}" draggable="true" class="draggable collidable" style="left:${this.posX}px; top:${this.posY}px; width:${this.width}px; height:${this.height}px; background-image:url(./images/crates/${this.texture});"></div>`;
    }
}
/*
* Drag Area Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

// This class acts as the view of the editor
// In charge of updating the visual representation of the level
class DragArea {
	constructor(){
		this.container = $('#drag-area');
		this.collidables = [];
		this.birds = [];
		this.setup();
	}

	// Makes the Drag Area a "Drop Target" for the draggable objects
	setup(){
		this.container.on('dragenter', event => event.preventDefault());
		this.container.on('dragover', event => event.preventDefault());
	}

	addCollidable(htmlCollidableElement) {
		this.container.append(htmlCollidableElement);
		this.collidables = this.container.children('div.draggable.collidable');
	}

	updateCollidable(collidable) {

		$(this.collidables[collidable.id]).css({
			width : `${collidable.width}px`,
			height : `${collidable.height}px`,
			backgroundImage : `url(./images/crates/${collidable.texture})`
		});
	}

	getSize() {
		return {
			width : this.container.width(),
			height : this.container.height()
		}
	}

	/*
		The listeners have to be reloadad everytime there is a change in the model
		for instance if everything in the view gets deleted
	*/

	reloadCollidablesListeners() {
		$.each(this.collidables, (index , collidable) => {
            $(collidable).on('click', event => this.highlightSelected(event));
            $(collidable).on('dragstart', event => this.setInitialMouseCoordinates(event));
            $(collidable).on('drag', event => this.dragElement(event));
        });
	}

	addBird(htmlBirdElement) {
		this.container.append(htmlBirdElement);
		this.birds = this.container.children('div.draggable.bird');
	}

	reloadBirdsListeners() {
		$.each(this.birds, (index , bird) => {
            $(bird).on('dragstart', event => this.setInitialMouseCoordinates(event));
            $(bird).on('drag', event => this.dragElement(event));
        });
	}

	addCannon(htmlCannonElement) {        
		this.container.append(htmlCannonElement);
		this.cannon = $('div.draggable.cannon');
		this.cannon.on('dragstart', event => this.setInitialMouseCoordinates(event));
        this.cannon.on('drag', event => this.dragElement(event));
	}

	highlightSelected(event) {
		event.preventDefault();
		let collidable = $(event.target);
		collidable.siblings('div.collidable').removeClass('highlighted');
		collidable.addClass('highlighted');
		// Create a custom event to broadcast to the object inspector 
		// Read the MDN Documentation on how to implement this on:
		// https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events#Creating_custom_events
		let collidableClickEvent = new CustomEvent('collidableclicked', { 
			detail : collidable.data('id'),
			bubbles : true,
			cancelable : true
		});
		event.target.dispatchEvent(collidableClickEvent);
	}

	clearHighlighted() {
		$('div.collidable.highlighted').removeClass('highlighted');
	}

	/*
		Algorithm to make the collidables update its position when dragged
	*/

	setInitialMouseCoordinates(event) {
		this.startCoordX = event.clientX;
		this.startCoordY = event.clientY;
	}

	dragElement(event) {
		event.preventDefault();
		let dragElement = event.target;
		// Get the coordinates between the initial and the new ones
		let diffX = this.startCoordX - event.clientX;
		let diffY = this.startCoordY - event.clientY;
		// Update the new starting coordinates
		this.startCoordX = event.clientX;
		this.startCoordY = event.clientY;
		// Sets the style on the elements
		dragElement.style.left = `${dragElement.offsetLeft - diffX}px`;
		dragElement.style.top = `${dragElement.offsetTop - diffY}px`;

		return dragElement;
	}

	changeBackground(image) {
		this.container.css('background-image', `url(./images/backgrounds/${image})`)
	}

	reset(){
		this.container.html('');
	}

	// Creates the view with a levele that was loaded by the user
	populateWithLevelData(level) {
		for (let i = 0;i < level.collidables.length; i++) {
			let newCollidable = new Collidable(level.collidables[i]).convertToHTML();
			this.addCollidable(newCollidable);
			if(i == level.collidables.length - 1) {
				this.reloadCollidablesListeners();
			}
		}

		for (let i = 0;i < level.birds.length; i++) {
			let newBird = new Bird(level.birds[i]).convertToHTML();
			this.addBird(newBird);
			if(i == level.birds.length - 1) {
				this.reloadBirdsListeners();
			}
		}
	}
}
/*
* Editor Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

// Manages the details of the collidables classes, that will define the physics behavior
class ObjectInspector {
	constructor(){
		this.objectInspectorForm = $('#object-inspector-form');
		this.texturesPulldown = $('#textures-pulldown');
		this.objectInspectorForm.hide();
	}

	fillForm(collidable) {
		this.objectInspectorForm.show();
		$('input[name=id]').val(collidable.id);
		$('input[name=height]').val(collidable.height);
		$('input[name=width]').val(collidable.width);
		$('input[name=mass]').val(collidable.mass);
		$('input[name=bounce]').val(collidable.bounce);
		$('input[name=friction]').val(collidable.friction);
		this.texturesPulldown.val(collidable.texture);
	}

	populateCollidablesTextures(textures) {
		let markup = '';
        textures.forEach(texture => {
            markup += `<option value="${texture}">${texture}</option>`;
        });
		this.texturesPulldown.html(markup);
	}
}
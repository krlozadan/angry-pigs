/*
* Level Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

// Represents the model editor. This is what is get saved to the server
class Level {
    constructor(levelObj = null) {
        // Creates a new clean Level
        if(!levelObj) {
            this.name = '';
            this.collidablesCount = 0;
            this.birdsCount = 0;
            this.editorScreenSize = {
                width : 0,
                height : 0
            };
            this.ammo = 1;
            this.background = 'beach.jpg';
            this.cannon = {
                posX : 30,
                posY : 30
            }
            this.collidables = [];
            this.birds = [];
        } else { // Fills the level information with a previuosly created level
            this.name = levelObj.name;
            this.birdsCount = levelObj.birdsCount;
            this.collidablesCount = levelObj.collidablesCount;
            this.ammo = levelObj.ammo;
            this.background = levelObj.background;
            this.cannon = levelObj.cannon;
            this.collidables = levelObj.collidables;
            this.birds = levelObj.birds;
        }
    }
    
    addCollidable(newCollidable){
        newCollidable.id = this.collidables.length;
        this.collidables.push(newCollidable);
        this.collidablesCount++;
    }

    addBird(newBird){
        newBird.id = this.birds.length;
        this.birds.push(newBird);
        this.birdsCount++;
    }
    
    // Updates the position of every draggable object in the model
    updateDraggable(event){
        event.preventDefault();
        let draggable = $(event.target);
        let index = draggable.data('id');
        if(draggable.hasClass('collidable')) {
            this.collidables[index].posX = parseInt(draggable.css('left'));
            this.collidables[index].posY = parseInt(draggable.css('top'));
        } else if (draggable.hasClass('bird')) {
            this.birds[index].posX = parseInt(draggable.css('left'));
            this.birds[index].posY = parseInt(draggable.css('top'));
        } else if (draggable.hasClass('cannon')) {
            this.cannon.posX = parseInt(draggable.css('left'));
            this.cannon.posY = parseInt(draggable.css('top'));
        }
    }

    // Updates the details of the colllidable sent by the object inspector
    updateCollidable(collidable) {
        this.collidables[collidable.id].bounce = Number(collidable.bounce);
        this.collidables[collidable.id].friction = Number(collidable.friction);
        this.collidables[collidable.id].height = Number(collidable.height);
        this.collidables[collidable.id].width = Number(collidable.width);
        this.collidables[collidable.id].mass = Number(collidable.mass);
        this.collidables[collidable.id].texture = collidable.texture;
    }

    updateLevelInfo(info) {
        this.name = info.name;
        this.collidablesCount = info.collidablesCount;
        this.birdsCount = info.birdsCount;
        this.ammo = info.ammo;
        this.background = info.background;
        this.editorScreenSize = info.editorScreenSize;
    }
}
/*
* Editor Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

class Editor {
    
    constructor() {
        // Members
        this.serverManager = new ServerManager();
        this.levelManager = new LevelManager();
        this.objectLibrary = new ObjectLibrary();
        this.objectInspector = new ObjectInspector();
        this.dragArea = new DragArea();
        this.level = new Level();
    }

    run() {
        // Attach all the events in the UI
        this.levelManager.loadLevelForm.submit((event) => this.loadLevel(event));
        this.levelManager.saveLevelForm.submit((event) => this.saveLevel(event));
        this.levelManager.newLevelBtn.click((event) => this.createNewLevel(event));
        this.levelManager.backgroundPulldown.change(event => this.changeLevelBackground($(event.target).val()));
        this.objectLibrary.addCollidableBtn.click((event) => this.addCollidableToEditor(event, new Collidable()));
        this.objectLibrary.addBirdBtn.click((event) => this.addBirdToEditor(event, new Bird()));
        this.dragArea.container.click(event => this.clearObjectInspector(event));
        this.dragArea.container.on('drop', event => this.level.updateDraggable(event));
        this.dragArea.container.on('collidableclicked', event => this.objectInspector.fillForm(this.level.collidables[event.detail]));
        this.objectInspector.objectInspectorForm.submit(event => this.updateCollidableDetails(event));
        // Places the cannon in the drag area for the first time
        this.dragArea.addCannon(new Cannon().convertToHTML());
        // Pulls levels, backgrounds and textures from server
        this.getLevelsList();
        this.getBackgroundImages();
        this.getCollidablesTextures();
    }
    
    // Pulls and populate the level names to the list 
    getLevelsList() {
        this.serverManager.getLevels()
        .then((data, status, jqxhr) => this.levelManager.populateLevels(data.levels))
        .catch(error => alert('Error downloading the levels list'));
    }

    // Pulls the differetn options for the background images
    getBackgroundImages() {
        this.serverManager.getBackgroundImages()
        .then((data, status, jqxhr) => this.levelManager.populateBackgroundImages(data.backgrounds))
        .catch(error => alert('Error downloading the background images'));
    }

    // Pulls and populate options that the user has for the collidables
    getCollidablesTextures() {
        this.serverManager.getCollidablesTextures()
        .then((data, status, jqxhr) => this.objectInspector.populateCollidablesTextures(data.textures))
        .catch(error => alert('Error downloading the texture images'));
    }

    // Fetches a level object from the server and bootstraps it to the editor
    loadLevel(event) {
        event.preventDefault();
        let levelName = $(event.target).serialize();
        this.serverManager.loadLevel(levelName)
        .then((data, status, jqxhr) => {
            this.level = new Level(data);
            this.levelManager.fillForm(this.level);
            this.dragArea.reset();
            this.changeLevelBackground(this.level.background);
            this.dragArea.addCannon(new Cannon(this.level.cannon).convertToHTML());
            this.dragArea.populateWithLevelData(this.level);
        })
        .catch(error => alert('Error downloading the texture images'));
    }

    // Sends the level information to the server to create or update a level
    saveLevel(event) {
        event.preventDefault();
        let arrayValues = $(event.target).serializeArray();
        let levelInfoOpts = {};
        arrayValues.forEach(input => levelInfoOpts[input.name] = input.value);

        // Validate and sanitize the name input
        levelInfoOpts.name = levelInfoOpts.name.trim();
        const levelNameRegex = new RegExp('^([A-Za-z\\d])+$');
        if (levelNameRegex.test(levelInfoOpts.name)) {
            levelInfoOpts['editorScreenSize'] = this.dragArea.getSize(); // Adds the screen size
            this.level.updateLevelInfo(levelInfoOpts);
            this.serverManager.saveLevel(this.level)
            .then((data, status, jqxhr) => {
                this.getLevelsList();
                alert(data.message);
            })
            .catch(error => alert('Error saving to the server', error));
        } else {
            alert('The name of the file can only accept letters and numbers, with no spaces');
        }
    }

    // Resets the whole editor to the initial state
    createNewLevel(event) {
        event.preventDefault();
        this.level = new Level();
        this.levelManager.fillForm(this.level);
        this.dragArea.reset();
        this.dragArea.addCannon(new Cannon().convertToHTML());
        this.dragArea.clearHighlighted();
        this.changeLevelBackground(this.level.background);
        this.objectInspector.objectInspectorForm.hide();
    }

    // Manages the collidables being added to the view and the model of the level
    addCollidableToEditor(event, collidable, reloadListeners = true) {
        event.preventDefault();
        this.level.addCollidable(collidable);
        this.levelManager.updateCollidablesCount(this.level.collidablesCount);
        this.dragArea.addCollidable(collidable.convertToHTML());
        if(reloadListeners) {
            this.dragArea.reloadCollidablesListeners();
        }
    }

    // Manages the birds being added to the view and the model of the level
    addBirdToEditor(event, bird, reloadListeners = true) {
        event.preventDefault();   
        this.level.addBird(bird);
        this.levelManager.updateBirdsCount(this.level.birdsCount);
        this.dragArea.addBird(bird.convertToHTML());
        if(reloadListeners) {
            this.dragArea.reloadBirdsListeners();
        }
    }
    
    // Updates the backgorund image depending in the user selection
    changeLevelBackground(image) {
        this.level.background = image;
        this.dragArea.changeBackground(image);
    }

    // Updates the collidables details sent from the inspector to the model and the view
    updateCollidableDetails(event){
        event.preventDefault();
        let arrayValues = $(event.target).serializeArray();
        let collidableObj = {};
        arrayValues.forEach(input => collidableObj[input.name] = input.value);
        this.level.updateCollidable(collidableObj);
        this.dragArea.updateCollidable(collidableObj);
    }

    // Hides the inspector details when the user clicks away from a collidable
    clearObjectInspector(event) {
        event.preventDefault();
        if(!event.target.classList.contains('collidable')){
            this.dragArea.clearHighlighted();
            this.objectInspector.objectInspectorForm.slideUp();
        }
    }
}
/*
* Object Library Class
* @author: Copyright (C) 2018 Carlos Adan Cortes De la Fuente - All Rights Reserved
* @email: krlozadan@gmail.com
* @date: 29/03/2018
*/
'use strict';

class ObjectLibrary {
	constructor(){
		this.addCollidableBtn = $('#add-collidable-btn');
		this.addBirdBtn = $('#add-bird-btn');
	}
}